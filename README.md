![header image](images/header.png)
# Give your curves the caps they deserve

I’ve always wanted to put rounded caps on my curves in Blender for some motion graphics animations, but there was no way I could find out about, so I’ve developed my own method, which works, but has its limitations.
One of these limitations was the tedious set-up process, which is why I wrote this script to automate it.

# How it works

Read [this article](https://medium.com/@amxmln/creating-caps-for-animated-curves-in-blender-2-8-d303b0025eba) for some more information on how the technique works in the background and some limitations that come with it.

The script works as follows:

* Add a curve, a bevel object, and a cap (preferably with it’s origin set at the bottom edge for the best results)
* Select the bevel object and the curve so the curve is the active object
* Copy/paste the script into a Blender text-editor and hit "Run Script"
* The curve should now have the bevel object applied to it (use Alt+S in edit-mode to shrink the curve’s radius and make the bevel smaller)
* Now select the cap and the curve so the curve again is the active object
* Run the script again
* The caps are automagically added to the curve as children and placed in the right positions
* Change the Bevel Start and Bevel End properties of the curve to see more magic happen (the caps should always follow the curve)
* Make something awesome


# Ideas

- [ ] Turn into an operator that’s callable from the UI
- [ ] Allow differring start and end caps (based on name?)
- [ ] Allow capifying multiple objects at the same time
- [ ] Make setting bevel_object and capifying a single step
