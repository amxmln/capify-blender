import bpy

selection = bpy.context.selected_objects
curve = bpy.context.active_object

def capify(cap, end = False, cyclic = False):
    # Create capCopy as a linked duplicate of cap
    capCopy = cap.copy()

    if not end:
        capCopy.name = 'StartCap'
    else:
        capCopy.name = 'EndCap'

    # Create Constraints
    limitLocation = capCopy.constraints.new('LIMIT_LOCATION')
    limitRotation = capCopy.constraints.new('LIMIT_ROTATION')
    limitScale = capCopy.constraints.new('LIMIT_SCALE')
    followPath = capCopy.constraints.new('FOLLOW_PATH')

    limitLocation.use_max_x = True
    limitLocation.use_max_y = True
    limitLocation.use_max_z = True
    limitLocation.use_min_x = True
    limitLocation.use_min_y = True
    limitLocation.use_min_z = True
    limitLocation.use_transform_limit = True

    limitRotation.use_limit_x = True
    limitRotation.use_limit_y = True
    limitRotation.use_limit_z = True
    limitRotation.use_transform_limit = True

    limitScale.use_max_x = True
    limitScale.use_max_y = True
    limitScale.use_max_z = True
    limitScale.use_min_x = True
    limitScale.use_min_y = True
    limitScale.use_min_z = True
    limitScale.max_x = 1.0
    limitScale.max_y = 1.0
    limitScale.max_z = 1.0
    limitScale.min_x = 1.0
    limitScale.min_y = 1.0
    limitScale.min_z = 1.0
    limitScale.use_transform_limit = True

    if not end:
        followPath.forward_axis = 'TRACK_NEGATIVE_Y'
    else:
        followPath.forward_axis = 'FORWARD_Y'

    followPath.target = curve
    followPath.up_axis = 'UP_Z'
    followPath.use_curve_follow = True
    followPath.use_curve_radius = True
    followPath.use_fixed_location = True

    offsetDriver = followPath.driver_add('offset_factor').driver
    offsetDriver.type = 'AVERAGE'
    var = offsetDriver.variables.new()
    var.targets[0].id_type = 'CURVE'
    var.targets[0].id = curve.data

    if not end:
        var.targets[0].data_path = 'bevel_factor_start'
    else:
        var.targets[0].data_path = 'bevel_factor_end'

    hideDriver = capCopy.driver_add('hide_render').driver

    hideDriver.type = 'SCRIPTED'

    if not cyclic:
        hideDriver.expression = '(start == 0 and end == 0) or (start == 1 and end == 1)' # non-cyclic
    else:
        hideDriver.expression = '(start == 0 and end == 0) or (start == 0 and end == 1) or (start == 1 and end == 1)' # faked cyclic (caps disappear if start and end have the same value)

    start = hideDriver.variables.new()
    end = hideDriver.variables.new()

    start.name = 'start'
    end.name = 'end'

    start.targets[0].id_type = 'CURVE'
    start.targets[0].id = curve.data
    start.targets[0].data_path = 'bevel_factor_start'

    end.targets[0].id_type = 'CURVE'
    end.targets[0].id = curve.data
    end.targets[0].data_path = 'bevel_factor_end'

    return capCopy

if curve and len(selection) >= 2 and curve.data.bevel_object == None:
    # We have to set the bevel object first to give it a stroke
    if selection[0] == curve:
        bevelObject = selection[1]
    else:
        bevelObject = selection[0]

    curve.data.bevel_object = bevelObject
    curve.data.bevel_factor_mapping_start = 'SPLINE'
    curve.data.bevel_factor_mapping_end = 'SPLINE'

elif len(selection) >= 2:
    # We can start adding the caps
    if selection[0] == curve:
        cap = selection[1]
    else:
        cap = selection[0]

    # Create cap objects (to fake cyclic add cyclic=True to the arguments)
    startCap = capify(cap)
    endCap = capify(cap, True)

    # Parent Caps to Curve
    startCap.parent = curve
    endCap.parent = curve

    # Add Objects to currenct collection
    # BUG: if they aren’t added to the same one as the curve, they don't show in the right place until bevel_factor is changed
    # Maybe find a way to find out what collection curve is in?
    bpy.context.collection.objects.link(startCap)
    bpy.context.collection.objects.link(endCap)

else:
    print('You need at least two objects selected!')
